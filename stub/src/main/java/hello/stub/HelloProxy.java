package hello.stub;
import cliffberg.utilities.HttpUtilities;
import java.net.URL;
public class HelloProxy implements HelloStub {
	private String serviceURL = null;
	public HelloProxy() {
		this.serviceURL = System.getenv("HELLO_SERVICE_URL");
		if (serviceURL == null) throw new RuntimeException("HELLO_SERVICE_URL not set");
	}
	public String hello() throws Exception {
		String response = HttpUtilities.getHttpResponseAsString(
			new URL(this.serviceURL + "/hello"));

		if ((response == null) || response.equals(""))
			throw new RuntimeException(
			"Internal server error: empty response received from server");

		return response;
	}
}
