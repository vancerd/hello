package hello.stub;
public class HelloProxyFactory implements HelloFactory {
	public HelloStub createHello() {
		return new HelloProxy();
	}
}
