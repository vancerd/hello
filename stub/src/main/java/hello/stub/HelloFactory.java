package hello.stub;
public interface HelloFactory {
	public HelloStub createHello();
}
