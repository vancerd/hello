package hello.cdk;

import software.amazon.awscdk.core.App;
import software.amazon.awscdk.core.StackProps;
import software.amazon.awscdk.core.Environment;

public class HelloApp {
	public static void main(final String[] args) {

		App app = new App();
		new HelloStack(app, "HelloTestStack");
		app.synth();
	}
}
