package hello.cdk;

import software.amazon.awscdk.core.Stack;
import software.amazon.awscdk.services.ecr.Repository;
import software.amazon.awscdk.services.ecs.Cluster;
import software.amazon.awscdk.services.ecs.ContainerImage;
import software.amazon.awscdk.services.ecs.CloudMapOptions;
import software.amazon.awscdk.services.ecs.ContainerDefinitionOptions;
import software.amazon.awscdk.services.ecs.PortMapping;
import software.amazon.awscdk.services.ecs.ContainerDefinitionOptions;
import software.amazon.awscdk.services.ecs.ContainerDefinition;
import software.amazon.awscdk.services.ecs.AppMeshProxyConfiguration;
import software.amazon.awscdk.services.ecs.FargateTaskDefinition;
import software.amazon.awscdk.services.ecs.AppMeshProxyConfigurationProps;
import software.amazon.awscdk.services.ecs.patterns.ApplicationLoadBalancedFargateService;
import software.amazon.awscdk.services.elasticloadbalancingv2.ApplicationTargetGroup;
import software.amazon.awscdk.services.servicediscovery.PrivateDnsNamespace;

import java.util.Map;
import java.util.HashMap;
import java.util.Arrays;

public class Hello {

        private ApplicationLoadBalancedFargateService albfs;

    public Hello(final Stack stack, final Cluster cluster, final String id,
                String serviceName, String weGoodFactoryClassName,
                String weGoodServiceURL, PrivateDnsNamespace privateDnsNamespace) {

                /* Pass name of the WeGood stub factory class to the container env: */
                Map containerEnvMap = new HashMap();
                containerEnvMap.put("WEGOOD_FACTORY_CLASS_NAME", weGoodFactoryClassName);
                containerEnvMap.put("WEGOOD_SERVICE_URL", weGoodServiceURL);

                /* Configure the Fargate Hello AppMesh proxy
                */
                FargateTaskDefinition appMeshProxy_hello = FargateTaskDefinition.Builder
                        .create(stack, "proxy_hello")
                        .proxyConfiguration(AppMeshProxyConfiguration.Builder.create()
                                .containerName("envoy")
                                .properties(AppMeshProxyConfigurationProps.builder()
                                        .appPorts(Arrays.asList(4567))
                                        .proxyIngressPort(15000)
                                        .proxyEgressPort(15001)
                                        .ignoredUid(1337)
                                        .build())
                                .build())
                        .build();

                /* Add Hello container to the Hello task definition
                */
                ContainerDefinition containerDefinition_hello = appMeshProxy_hello.addContainer(
                        "hello", ContainerDefinitionOptions.builder()
                                .image(ContainerImage.fromRegistry("cliffberg/hello"))
                                .environment(containerEnvMap)
                                .build());

                /* Set Hello container ports.
                One must explicitly set ports when one creates those from CDK.
                */
                containerDefinition_hello.addPortMappings(PortMapping.builder()
                        .containerPort(4567)
                        .hostPort(4567)
                        .build());

                /* Add envoy container to taskdefinition */

                Map<String, String> envoyMap  = new HashMap<String, String>() {{
                        put("APPMESH_VIRTUAL_NODE_NAME", "mesh/moody-mesh/virtualNode/hello-vn");
                        }};

                appMeshProxy_hello.addContainer(
                        "envoy", ContainerDefinitionOptions.builder()
                                .image(ContainerImage.fromEcrRepository(Repository.fromRepositoryArn(
                                        stack, "envoy", "arn:aws:ecr:us-west-2:840364872350:repository/aws-appmesh-envoy"),"v1.12.1.0-prod"))
                                .environment(envoyMap)
                                .user("1337")
                                .build());

                /* Create a load-balanced Fargate service for hello and make it public */
                this.albfs = ApplicationLoadBalancedFargateService.Builder.create(stack, id)
                        .serviceName(serviceName)
                        .cluster(cluster)           // Required
                        .cpu(512)                   // Default is 256
                        .desiredCount(2)            // Default is 1
                        .cloudMapOptions(CloudMapOptions.builder()
                                .cloudMapNamespace(privateDnsNamespace)
                                .name("hello")
                                .build())
                        .memoryLimitMiB(1024)       // Default is 512
                        .publicLoadBalancer(true)   // Default is false
                        .taskDefinition(appMeshProxy_hello)
                        .build();
        }

        public ApplicationLoadBalancedFargateService getALBFS() { return albfs; }
}
