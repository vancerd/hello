package hello;

import static spark.Spark.*;
import wegood.stub.WeGoodFactory;
import wegood.stub.WeGoodStub;
import java.time.LocalTime;

public class HelloWorld {
	WeGoodStub weGood = null;
	public static void main(String[] args) throws Exception{
		HelloWorld helloWorld = new HelloWorld();
		get("/hello", (req, res) -> helloWorld.sayHi());
	}

	public HelloWorld() throws Exception {
		String weGoodFactoryClassName =
			System.getenv("WEGOOD_FACTORY_CLASS_NAME");
		if (weGoodFactoryClassName == null) throw new Exception(
			"WEGOOD_FACTORY_CLASS_NAME not specified");
		WeGoodFactory factory = null;
		factory = (WeGoodFactory)(Class.forName(weGoodFactoryClassName)
			.getDeclaredConstructor().newInstance());
		this.weGood = factory.createWeGood();
	}

	public String sayHi() throws Exception {
		boolean weGoodResponse = false;
		for (int i = 0; i < 3; i++) {
			try { weGoodResponse = weGood.areWeGood(); } catch (Exception ex) {
				new Timer(1000, () -> {}); // wait one second
				if (i == 2) throw ex; // too many times - re-throw the exception
				continue; // try again
			}
			break;
		}
		if (weGoodResponse) return "Hello World!";
	        else return "Goodbye";
	}

	/** For testing only */
	public void setTime(LocalTime time) throws Exception {
		this.weGood.setTime(time);
	}


	private class Timer {
		Thread t = new Thread();
		boolean isSleeping = false;
		Timer(int milliseconds, Runnable doIfInterrupted) {
			try {
				t.sleep(milliseconds);
				isSleeping = true;
				t.join();
			}
			catch (InterruptedException ex) { doIfInterrupted.run(); }
		}
		public void cancel() { t.interrupt(); isSleeping = false; }
		boolean isWaiting() { return this.isSleeping; }
	}


}
