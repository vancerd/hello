package hello;
import static org.junit.Assert.*;
import org.junit.Test;
import java.time.LocalTime;

public class TestHelloWorld {

	@Test
	public void testSayHi() throws Exception{
		HelloWorld h = new HelloWorld();
		LocalTime time = LocalTime.of(18, 1);
		h.setTime(time);
		String result = h.sayHi();
		assertEquals("Hello World!", result);
	}
}
