package hello.storytests;

// Import the Hello stub:
import hello.stub.HelloFactory;
import hello.stub.HelloStub;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.*;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;

import java.time.LocalTime;

// Import the Hello HelloStub
import hello.stub.HelloFactory;
import hello.stub.HelloStub;

// Import the WeGood HelloStub
import wegood.stub.WeGoodFactory;
import wegood.stub.WeGoodStub;

public class BasicIT {
//    private String json = null;
    String helloResponse = null;
    HelloStub hello = null;
    WeGoodStub wegood = null;
    public BasicIT() throws Exception {
        String helloFactoryClassName = System.getenv("HELLO_FACTORY_CLASS_NAME");
        if (helloFactoryClassName == null) throw new Exception(
          "Env variable HELLO_FACTORY_CLASS_NAME is not defined");
        HelloFactory helloFactory = (HelloFactory)
          (Class.forName(helloFactoryClassName).getDeclaredConstructor().newInstance());
        this.hello = helloFactory.createHello();

        String wegoodFactoryClassName = System.getenv("WEGOOD_FACTORY_CLASS_NAME");
        if (wegoodFactoryClassName == null) throw new Exception(
          "Env variable WEGOOD_FACTORY_CLASS_NAME is not defined");
        WeGoodFactory weGoodFactory = (WeGoodFactory)
          (Class.forName(wegoodFactoryClassName).getDeclaredConstructor().newInstance());
        this.wegood = weGoodFactory.createWeGood();
        }

        @Given("the time is {string}")
        public void set_time(String timestr) throws Exception {
          LocalTime localTime = LocalTime.parse(timestr);
          this.wegood.setTime(localTime);
        }

        @When("I call HelloWorld")
        public void run_hello_world() throws Exception {
          this.helloResponse = hello.hello();
        }

        @Then("it returns {string}")
        public void it_displays_hello_world(String expectedResponse) throws Exception {
          assertEquals(expectedResponse, this.helloResponse);
        }
}
