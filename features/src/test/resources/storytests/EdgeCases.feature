@integration
Feature: Edge Cases
  Scenario: Just before noon
    Given the time is "11:59"
    When I call HelloWorld
    Then it returns "Goodbye"
  Scenario: At noon
    Given the time is "12:00"
    When I call HelloWorld
    Then it returns "Goodbye"
  Scenario: Just after noon
    Given the time is "12:01"
    When I call HelloWorld
    Then it returns "Goodbye"
