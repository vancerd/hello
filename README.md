# Why I Am Studying DevOps at Berg University

## There are basically 3 reasons:

1. So I can get rich
[![Show me the money](https://www.facingsouth.org/sites/default/files/images/gavel-money-2.jpg)](https://www.facingsouth.org/)

2. So I will be smart  
[![I want to be smart](https://winkgo.com/wp-content/uploads/2017/09/49-Funny-School-Memes-Not-Everyone-Likes-School-22.jpg)](https://winkgo.com/)

3. Chicks dig DevOps
[![For the chicks](https://cdn.pixabay.com/photo/2014/05/20/21/25/chicks-349035_960_720.jpg)](https://cdn.pixabay.com/)
